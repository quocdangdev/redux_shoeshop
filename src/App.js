import logo from './logo.svg';
import Ex_ShoeShop_Redux from './Ex_ShoeShop _REDUX/Ex_ShoeShop_Redux';
import './App.css';


function App() {
  return (
    <div className="App">
      <Ex_ShoeShop_Redux />
    </div>
  );
}

export default App;
