import React, { Component } from 'react'
import ItemShoe from './ItemShoe'
import DetailShoe from './DetailShoe'
import { connect } from 'react-redux'
class ListShoe extends Component {

    renderListShoe = () => {
        return this.props.ListShoe.map((item, index) => {
            return <ItemShoe
                handleClick={this.props.handleChangDetailShoe}
                data={item} key={index} />
        })
    }
    render() {
        console.log("🚀 ~ file: ListShoe.jsx ~ line 17 ~ ListShoe ~ render ~ props", this.props)
        return (
            <div className='row'>{this.renderListShoe()}
            </div>

        )
    }
}

let mapStateToProps = (state) => {
    return {
        ListShoe: state.shoeReducer.shoeArr
    }
}
export default connect(mapStateToProps)(ListShoe)
