import React, { Component } from 'react'
import { connect } from 'react-redux';

class DetailShoe extends Component {
    render() {
        let { id, name, shortDescription, price, image } = this.props.detail;
        return (
            <div className='row mt-5 alert-secondary p-5 text-left'>
                <img src={image} alt="" className='col-3' />
                <div className='col-9'>
                    <p>id:{id}</p>
                    <p>name:{name}</p>
                    <p>desc:{shortDescription}</p>
                    <p>price:{price}</p>
                </div>
            </div>
        )
    }
}
let mapStateToProps = (state) => {
    return {
        detail: state.shoeReducer.detail
    };
}


export default connect(mapStateToProps)(DetailShoe)
