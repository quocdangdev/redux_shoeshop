import { dataShoe } from "../../dataShos"
import * as actionTyPe from "../reducerc/constant/ShoeConstant"

let initialState = {
    shoeArr: dataShoe,
    detail: dataShoe[5],
    cart: [],
}

export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTyPe.ADD_TO_CART: {
            console.log(action.payload)
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
            })
            console.log(index)
            if (index == -1) {
                let cartItem = { ...action.payload, number: 1 }
                cloneCart.push(cartItem);
            }
            else {
                cloneCart[index].number++;
            }
            console.log(cloneCart)
            return { ...state, cart: cloneCart }
        }
        case actionTyPe.TANG_GIAM_SO_LUONG: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.idShoe;
            })
            if (index !== -1) {
                cloneCart[index].number = cloneCart[index].number + action.payload.soLuong
            }
            (cloneCart[index].number == 0) && cloneCart.splice(index, 1);
            return { ...state, cart: cloneCart }
        }
        case actionTyPe.XEM_CHI_TIET: {
            return { ...state, detail: action.payload }
        }
        default:
            return state;
    }
}