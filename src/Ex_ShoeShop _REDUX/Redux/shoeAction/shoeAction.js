import { XEM_CHI_TIET } from "../reducerc/constant/ShoeConstant"

export const changDetailAction = (value) => {
    return {
        type: XEM_CHI_TIET,
        payload: value
    }
}