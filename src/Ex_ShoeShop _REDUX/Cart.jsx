import React, { Component } from 'react'
import { connect } from 'react-redux'

class Cart extends Component {
    renderTbody = () => {
        return this.props.giohang?.map((item) => {
            return <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td><img style={{ width: "50px" }} src={item.image} alt="" /></td>
                <td>{item.price * item.number} $$</td>
                <td>
                    <button onClick={() => { this.props.handleTangGiamSoLuong(item.id, +1) }} className='btn btn-danger'>+</button>
                    <span>{item.number}</span>
                    <button onClick={() => { this.props.handleTangGiamSoLuong(item.id, -1) }} className='btn btn-warning'>-</button>
                </td>
            </tr>
        })
    }
    render() {
        return (
            <table className='table table-sm table-dark'>
                <thead>
                    <tr>
                        <td>id</td>
                        <td>name</td>
                        <td>img</td>
                        <td>price</td>
                        <td>quantity</td>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTbody()}
                </tbody>
            </table>
        )
    }
}
let mapStateToProps = (state) => {
    return {
        giohang: state.shoeReducer.cart,
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleTangGiamSoLuong: (id, soLuong) => {
            let action = {
                type: "TANG_GIAM_SO_LUONG",
                payload: {
                    idShoe: id,
                    soLuong: soLuong,
                }
            }
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart)



