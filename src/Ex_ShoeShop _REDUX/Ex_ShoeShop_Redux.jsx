import React, { Component } from 'react'
import { dataShoe } from './dataShos';
import ItemShoe from './ItemShoe';
import DetailShoe from './DetailShoe';
import Cart from './Cart';
import ListShoe from './ListShoe';

export default class Ex_ShoeShop_Redux extends Component {
    render() {
        return (

            <div className="container">
                <Cart />
                <ListShoe
                />
                <DetailShoe
                />
            </div>
        );
    }
}
