import React, { Component } from 'react'
import { connect } from 'react-redux';
import { changDetailAction } from './Redux/shoeAction/shoeAction';
class ItemShoe extends Component {
    render() {
        console.log(this.props)
        let { image, name, price } = this.props.data;
        return (
            < div className='col-3 p-1' >
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <p className="card-text">giá :{price}$</p>
                    </div>
                    <div>
                        <button onClick={() => {
                            this.props.handleChangDetail(this.props.data)
                        }} className='btn btn-secondary'>xem chi tiết</button>
                        <button onClick={() => {
                            this.props.handleAddToCart(this.props.data)
                        }} className='btn btn-danger'>thêm mua </button>
                    </div>
                </div>
            </ div>
        )
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleAddToCart: (shoe) => {
            let action = {
                type: "ADD_TO_CART",
                payload: shoe,
            }
            dispatch(action);
        },
        handleChangDetail: (shoe) => {

            dispatch(changDetailAction(shoe));
        }
    }
}
export default connect(null, mapDispatchToProps)(ItemShoe)

